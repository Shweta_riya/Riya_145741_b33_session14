<!DOCTYPE html>
<html lang="en">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../../asset/bootstrap/css/bootstrap.min.css">
    <script src="../../asset/bootstrap/jquery.min.js"></script>
    <script src="../../asset/bootstrap/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
    <h2>Birthday-Add</h2>
    <form>
        <div class="form-group">
            <label for="Birthday">Name:</label>
            <input type="birthday" class="form-control" id="Name" placeholder="Enter name here">
        </div>

        <div class="form-group">
            <label for="birthdate">Date:</label>
            <input type="birthdate" class="form-control" id="date" placeholder="Enter birthdate">
        </div>

        <button type="submit" class="btn btn-primary">Add</button>

        <button type="submit" class="btn btn-default">Add/save</button>

    </form>
</div>

</body>
</html>