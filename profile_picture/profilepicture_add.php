<!DOCTYPE html>
<html lang="en" xmlns="http://www.w3.org/1999/html">
<head>
    <title>Bootstrap Example</title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <link rel="stylesheet" href="../asset/bootstrap/css/bootstrap.min.css">
    <script src="../asset/bootstrap/jquery.min.js"></script>
    <script src="../asset/bootstrap/js/bootstrap.min.js"></script>
</head>
<body>
<div class="container">
    <h2>profilepicture-Add</h2>
    <form>
        <div class="form-group">
            <label for="profilepicture">profilepicture:</label>
            <input type="profilepicture" class="form-control" id="profilepicture" placeholder="upload your profile pic">
        </div>

        <div class="form-group">
            <label for="upload">upload:</label>
            <input type="file" class="form-control" id="upload" placeholder="upload your profile picture">
        </div>

        <button type="upload" class="btn btn-primary" >upload</button>


    </form>
</div>

</body>
</html>